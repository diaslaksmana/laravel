<style>
    .home-active{
        font-weight: bold;
    }
</style>
@extends('layout/main')

@section('title', 'Laravel Project')

@section('content')
<div id="content" class="content">
    <div class="container">
        <h1 class="row my-5">
            <div class="col-12 col-sm-12 col-md-4 col-lg-5">
                <div class="slider">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="/assets/img/slide1.jpg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/assets/img/slide2.jpeg" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="/assets/img/slide3.jpeg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-7">
                <h4>News</h4>
                <div class="highlight-news mb-2">
                    <h5><b>Lorem Ipsum is simply dummy text </b></h5>
                    <p>
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </p>
                    <small>20 Agustus 2019, Oleh Admin</small>
                </div>
                <div class="highlight-news mb-2">
                    <h5><b>Lorem Ipsum is simply dummy text </b></h5>
                    <p>
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </p>
                    <small>20 Agustus 2019, Oleh Admin</small>
                </div>
                <div class="highlight-news mb-2">
                    <h5><b>Lorem Ipsum is simply dummy text </b></h5>
                    <p>
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </p>
                    <small>20 Agustus 2019, Oleh Admin</small>
                </div>
            </div>
        </h1>
    </div>
</div>
@endsection
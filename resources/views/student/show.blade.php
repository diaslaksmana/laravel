<style>
    .student-active {
        font-weight: bold;
    }
</style>
<!-- memanggil layout header -->
@extends('layout/main')

<!-- memanggil judul halaman -->
@section('title', 'Laravel Project || Detail Student')

<!-- isi konten/ halaman -->
@section('content')
<div id="content" class="content">
    <div class="container">
        <div class="d-flex bd-highlight my-4">
            <div class="mr-auto p-2 bd-highlight">
                <h3 class="">
                    List Students
                </h3>
            </div>
            <div class="p-2 bd-highlight">
                <a href="/students" class="btn btn-sm btn-light">
                    Back
                </a>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">NRP</th>
                    <th scope="col">Email</th>
                    <th scope="col">Jurusan</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$student->nama}}</td>
                    <td>{{$student->nrp}}</td>
                    <td>{{$student->email}}</td>
                    <td>{{$student->jurusan}}</td>
                    <td>
                        <a href="" class="badge badge-success">Edit</a>
                        <a href="" class="badge badge-danger">Hapus</a>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@home'); // memanggil halaman home lewat controller
Route::get('/about','PagesController@about'); // memanggil halaman about 
Route::get('/mahasiswa','MahasiswaController@index'); // memanggil halaman mahasiswa

//page students
Route::get('/students','StudentsController@index'); // memanggil halaman student
Route::get('/students/create','StudentsController@create');
Route::get('/students/{student}','StudentsController@show'); // memanggil halaman student
Route::post('/students','StudentsController@store'); 

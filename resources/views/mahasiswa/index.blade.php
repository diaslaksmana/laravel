<style>
    .student-active{
        font-weight: bold;
    }
</style>
<!-- memanggil layout header -->
@extends('layout/main')

<!-- memanggil judul halaman -->
@section('title', 'Laravel Project || Mahasiswa')

<!-- isi konten/ halaman -->
@section('content')
<div id="content" class="content">
    <div class="container">
        <h3 class="my-5">
            Daftar Mahasiswa
        </h3>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NRP</th>
                    <th scope="col">Email</th>
                    <th scope="col">Jurusan</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mahasiswa as $mhs)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$mhs->nama}}</td>
                    <td>{{$mhs->nrp}}</td>
                    <td>{{$mhs->email}}</td>
                    <td>{{$mhs->jurusan}}</td>
                    <td>
                        <a href="" class="badge badge-success">Edit</a>
                        <a href="" class="badge badge-danger">Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection
<style>
    .student-active {
        font-weight: bold;
    }
</style>
<!-- memanggil layout header -->
@extends('layout/main')

<!-- memanggil judul halaman -->
@section('title', 'From Add Student')

<!-- isi konten/ halaman -->
@section('content')
<div id="content" class="content">
    <div class="container">
        <div class="d-flex bd-highlight my-4">
            <div class="mr-auto p-2 bd-highlight">
                <h3 class="">
                    List Students
                </h3>
            </div>
            <div class="p-2 bd-highlight">
                <a href="/students" class="btn btn-sm btn-light">
                    Back
                </a>
            </div>
        </div>

        <div class="col-6">
            <form method="post" action="/students">
                @csrf
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" placeholder="Nama">
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">NRP</label>
                    <input type="text" class="form-control  @error('nrp') is-invalid @enderror" name="nrp" id="nrp" placeholder="NRP">
                    @error('nrp')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control  @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Jurusan</label>
                    <input type="text" class="form-control  @error('jurusan') is-invalid @enderror" name="jurusan" id="jurusan" placeholder="Jurusan">
                    @error('jurusan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>


    </div>
</div>
@endsection
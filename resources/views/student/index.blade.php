<style>
    .student-active {
        font-weight: bold;
    }
</style>
<!-- memanggil layout header -->
@extends('layout/main')

<!-- memanggil judul halaman -->
@section('title', 'Laravel Project || Students')

@if(Session::has('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Success!</strong> {{ Session::get('message', '') }}
    </div>
@endif

<!-- isi konten/ halaman -->
@section('content')
<div id="content" class="content">
    <div class="container">
        <h3 class="my-5">
            List Students
        </h3>
        <a href="/students/create" class="btn btn-primary btn-sm mb-3">Add New Student</a>
        <ul class="list-group col-12 col-sm-6 col-md-6">
            @foreach($students as $student)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                {{$student->nama}}
                <a href="/students/{{$student->id}}" class="badge badge-info">detail</a>
            </li>
            @endforeach
        </ul>

    </div>
</div>
@endsection